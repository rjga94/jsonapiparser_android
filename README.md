# JSON API Parser - Flatten complex JSON:API data

Init the parser in the Application class:

```
class MyApplications: Application() {
    
    override fun onCreate() {
        super.onCreate()
    
        val options = Options(JsonApiVersion.ONE_DOT_ZERO, true)
        JsonApiParser.initialize(options)
    }
}
```

Use this function to parse a JSON:API structured string:

```
val obj = JsonApiParser.getInstance().parse(raw)
```