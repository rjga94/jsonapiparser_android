package rjga.android.examples.jsonapiparser

import android.app.Application
import rjga.android.jsonapiparser.JsonApiParser
import rjga.android.jsonapiparser.JsonApiVersion
import rjga.android.jsonapiparser.Options

/**
 * Created by Ricardo on 2020-02-20.
jsonapiparser-android
 */
class MyApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        /*
          Specify the options to be used when parsing the JSON API
         */
        val options = Options()
        options.version = JsonApiVersion.ONE_DOT_ZERO
        options.parseLinks = true

        /*
          The initialize method must always be called before using the library in any way
          The application class is the best place for it
         */
        JsonApiParser.initialize(options)
    }

}