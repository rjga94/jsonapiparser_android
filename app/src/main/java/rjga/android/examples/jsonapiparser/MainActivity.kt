package rjga.android.examples.jsonapiparser

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.util.AttributeSet
import android.view.View
import rjga.android.examples.jsonapiparser.databinding.ActivityMainBinding
import rjga.android.jsonapiparser.JsonApiParser

class MainActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreateView(name: String, context: Context, attrs: AttributeSet) =
        ActivityMainBinding.inflate(layoutInflater).apply {
            buttonExampleSimple.setOnClickListener {

                /*
                      Simply call the parse method and you're done !
                      Make a breakpoint here to check the result
                     */
                val parsedJson = JsonApiParser.getInstance().parse(JsonExamples.SIMPLE)

                textViewMessage.text = "JSON parsed ! Check the logs for the result"

            }

            buttonExampleComplex.setOnClickListener {

                /*
                      Simply call the parse method and you're done !
                      Make a breakpoint here to check the result
                     */
                val parsedJson = JsonApiParser.getInstance().parse(JsonExamples.COMPLEX)

                textViewMessage.text = "JSON parsed ! Check the logs for the result"

            }
        }.root
}
