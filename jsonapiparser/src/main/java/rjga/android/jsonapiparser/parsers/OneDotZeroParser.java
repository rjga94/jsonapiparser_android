package rjga.android.jsonapiparser.parsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import rjga.android.jsonapiparser.Options;

public class OneDotZeroParser implements IParser {

    @Override
    public JSONArray parse(JSONObject obj, Options options) {
        JSONArray resultArray = new JSONArray();

        try {
            JSONArray dataArray = obj.getJSONArray("data");
            JSONArray includedArray = obj.optJSONArray("included");

            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject dataObj = dataArray.getJSONObject(i);

                JSONObject result = new JSONObject();

                result.put("id", dataObj.getString("id"));
                result.put("type", dataObj.getString("type"));

                if (options.getParseLinks()) parseLinks(dataObj, result);

                JSONObject attributes = dataObj.optJSONObject("attributes");
                if (attributes == null) {
                    attributes = new JSONObject();
                    dataObj.put("attributes", attributes);
                }

                Iterator<String> attributesIterator = attributes.keys();
                while (attributesIterator.hasNext()) {
                    String key = attributesIterator.next();
                    result.put(key, attributes.get(key));
                }

                JSONObject relationshipsObj = dataObj.optJSONObject("relationships");
                if (relationshipsObj != null && includedArray != null) {
                    parseRelationships(relationshipsObj, includedArray, options, result);
                }

                resultArray.put(result);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return resultArray;
    }

    private void parseLinks(JSONObject dataObj, JSONObject result) throws JSONException {
        JSONObject linksObj = dataObj.optJSONObject("links");
        if (linksObj != null) {
            JSONObject selfObj = linksObj.optJSONObject("self");
            if (selfObj != null) {
                String selfLink = selfObj.optString("href", "");
                if (!selfLink.isEmpty()) {
                    result.put("self_link", selfLink);
                }
            }
        }
    }

    private void parseRelationships(JSONObject relationshipsObj, JSONArray includedArray, Options options, JSONObject result) throws JSONException {
        Iterator<String> relationshipsIterator = relationshipsObj.keys();
        while (relationshipsIterator.hasNext()) {
            String relationshipKey = relationshipsIterator.next();
            JSONObject relationshipObj = relationshipsObj.getJSONObject(relationshipKey);

            JSONArray resultArray = new JSONArray();

            JSONArray dataArray = relationshipObj.optJSONArray("data");
            if (dataArray == null) {
                JSONArray _dataArray = new JSONArray();

                JSONObject _dataObj = relationshipObj.optJSONObject("data");
                if (_dataObj == null) continue;

                _dataArray.put(relationshipObj.getJSONObject("data"));
                dataArray = _dataArray;
            }

            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject dataObj = dataArray.getJSONObject(i);

                for (int j = 0; j < includedArray.length(); j++) {
                    JSONObject includedObj = includedArray.getJSONObject(j);
                    String includedObjId = includedObj.getString("id");

                    if (includedObjId.equals(dataObj.getString("id"))) {

                        JSONObject resultObj = new JSONObject();

                        resultObj.put("id", dataObj.getString("id"));
                        resultObj.put("type", dataObj.getString("type"));

                        if (options.getParseLinks()) parseLinks(includedObj, resultObj);

                        JSONObject includedObjAttributes = includedObj.optJSONObject("attributes");
                        if (includedObjAttributes == null) {
                            includedObjAttributes = new JSONObject();
                            dataObj.put("attributes", includedObjAttributes);
                        }

                        Iterator<String> attributesIterator = includedObjAttributes.keys();
                        while (attributesIterator.hasNext()) {
                            String attributeKey = attributesIterator.next();
                            resultObj.put(attributeKey, includedObjAttributes.get(attributeKey));
                        }

                        JSONObject includedObjRelationships = includedObj.optJSONObject("relationships");
                        if (includedObjRelationships != null) {
                            parseRelationships(includedObjRelationships, includedArray, options, resultObj);
                        }

                        resultArray.put(resultObj);

                        break;
                    }
                }
            }

            if (resultArray.length() > 0) {
                if (resultArray.length() == 1) result.put(relationshipKey, resultArray.getJSONObject(0));
                else result.put(relationshipKey, resultArray);
            }
        }
    }
}
