package rjga.android.jsonapiparser.parsers;


import org.json.JSONArray;
import org.json.JSONObject;

import rjga.android.jsonapiparser.Options;

public interface IParser {
    JSONArray parse(JSONObject obj, Options options);
}
