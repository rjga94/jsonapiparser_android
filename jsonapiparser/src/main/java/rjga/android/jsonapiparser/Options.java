package rjga.android.jsonapiparser;

public class Options {
    private JsonApiVersion version;
    private boolean parseLinks;

    public Options() {
        this.version = JsonApiVersion.ONE_DOT_ZERO;
        this.parseLinks = false;
    }

    public Options(JsonApiVersion version, boolean parseLinks) {
        this.version = version;
        this.parseLinks = parseLinks;
    }

    public JsonApiVersion getVersion() {
        return version;
    }

    public void setVersion(JsonApiVersion version) {
        this.version = version;
    }

    public boolean getParseLinks() {
        return parseLinks;
    }

    public void setParseLinks(boolean parseLinks) {
        this.parseLinks = parseLinks;
    }
}
