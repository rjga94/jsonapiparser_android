package rjga.android.jsonapiparser;

public enum JsonApiVersion {
    ONE_DOT_ZERO(1.0);

    private Double version;

    JsonApiVersion(Double version) {
        this.version = version;
    }

    public Double toDouble() {
        return version;
    }
}
