package rjga.android.jsonapiparser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rjga.android.jsonapiparser.parsers.OneDotZeroParser;

public class JsonApiParser {
    private static Options options;
    private static JsonApiParser instance;

    public static JsonApiParser getInstance() {
        if (instance == null) throw new RuntimeException("JsonApiParser needs to be initialized first!");
        return instance;
    }

    public static void initialize() {
        Options options = new Options(JsonApiVersion.ONE_DOT_ZERO, false);
        initialize(options);
    }

    public static void initialize(Options options) {
        JsonApiParser.options = options;
        instance = new JsonApiParser();
    }

    public JSONArray parse(String json) {
        JSONArray result = new JSONArray();

        try {

            JSONObject obj = new JSONObject(json);

            JSONObject jsonApiObj = obj.getJSONObject("jsonapi");
            Double version = jsonApiObj.getDouble("version");

            if (!version.equals(options.getVersion().toDouble())) {
                throw new IllegalArgumentException("Detected JSON API version " + version + ", but trying to parse using version " + options.getVersion().toDouble());
            }

            if (version.equals(JsonApiVersion.ONE_DOT_ZERO.toDouble())) {
                result = new OneDotZeroParser().parse(obj, options);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }
}
